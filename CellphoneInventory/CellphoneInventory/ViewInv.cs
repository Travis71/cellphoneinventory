﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CellphoneInventory
{
    public partial class ViewInv : Form
    {
        private BindingSource bsCelular;
        private DataSet dsCelular;
        public ViewInv()
        {
            InitializeComponent();
            bsCelular = new BindingSource();
        }

        public DataSet DsCelular
        {
            get
            {
                return dsCelular;
            }

            set
            {
                dsCelular = value;
            }
        }

        private void ViewInv_Load(object sender, EventArgs e)
        {
            bsCelular.DataSource = DsCelular;
            bsCelular.DataMember = DsCelular.Tables["cellphone"].TableName;
            dgvCel.DataSource = bsCelular;
            dgvCel.AutoGenerateColumns = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            for (int index = 0; index < dgvCel.Rows.Count; index++) if (dgvCel.Rows[index].Selected == true) ;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dgvCel.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "Debe seleccionar una fila de la tabla para poder eliminar", "Mensaje de Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;

            DialogResult result = MessageBox.Show(this, "¿Realmente desea eliminar ese registro?", "Mensaje del Sistema", MessageBoxButtons.YesNo, MessageBoxIcon.Question);


            if (result == DialogResult.Yes)
            {
                DsCelular.Tables["cellphone"].Rows.Remove(drow);
                MessageBox.Show(this, "Registro eliminado satisfactoriamente", "Mensaje del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

    }
}


