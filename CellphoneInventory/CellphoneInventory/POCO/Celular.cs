﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CellphoneInventory.POCO
{
    class Celular
    {
        private string nombre;
        private string network;
        private string anuncio;
        private string estado;
        private string lanzamiento;
        private string dimension;
        private string peso;
        private string build;
        private string sim;
        private string d_tipo;
        private string d_tamano;
        private string d_resolucion;
        private string d_proteccion;
        private string plat_os;
        private string plat_chip;
        private string plat_cpu;
        private string plat_gpu;
        private string mem_cardslot;
        private string mem_internal;

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }

        public string Network
        {
            get
            {
                return network;
            }

            set
            {
                network = value;
            }
        }

        public string Anuncio
        {
            get
            {
                return anuncio;
            }

            set
            {
                anuncio = value;
            }
        }

        public string Estado
        {
            get
            {
                return estado;
            }

            set
            {
                estado = value;
            }
        }

        public string Lanzamiento
        {
            get
            {
                return lanzamiento;
            }

            set
            {
                lanzamiento = value;
            }
        }

        public string Dimension
        {
            get
            {
                return dimension;
            }

            set
            {
                dimension = value;
            }
        }

        public string Peso
        {
            get
            {
                return peso;
            }

            set
            {
                peso = value;
            }
        }

        public string Build
        {
            get
            {
                return build;
            }

            set
            {
                build = value;
            }
        }

        public string Sim
        {
            get
            {
                return sim;
            }

            set
            {
                sim = value;
            }
        }

        public string D_tipo
        {
            get
            {
                return d_tipo;
            }

            set
            {
                d_tipo = value;
            }
        }

        public string D_tamano
        {
            get
            {
                return d_tamano;
            }

            set
            {
                d_tamano = value;
            }
        }

        public string D_resolucion
        {
            get
            {
                return d_resolucion;
            }

            set
            {
                d_resolucion = value;
            }
        }

        public string D_proteccion
        {
            get
            {
                return d_proteccion;
            }

            set
            {
                d_proteccion = value;
            }
        }

        public string Plat_os
        {
            get
            {
                return plat_os;
            }

            set
            {
                plat_os = value;
            }
        }

        public string Plat_chip
        {
            get
            {
                return plat_chip;
            }

            set
            {
                plat_chip = value;
            }
        }

        public string Plat_cpu
        {
            get
            {
                return plat_cpu;
            }

            set
            {
                plat_cpu = value;
            }
        }

        public string Plat_gpu
        {
            get
            {
                return plat_gpu;
            }

            set
            {
                plat_gpu = value;
            }
        }

        public string Mem_cardslot
        {
            get
            {
                return mem_cardslot;
            }

            set
            {
                mem_cardslot = value;
            }
        }

        public string Mem_internal
        {
            get
            {
                return mem_internal;
            }

            set
            {
                mem_internal = value;
            }
        }
    }
}
